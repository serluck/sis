import Vue from 'vue'
import Router from 'vue-router'

import invoiceList from '../components/invoice/index'
import InvoiceForm from '../components/invoice/form'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/', component: invoiceList},
    {
      path: '/invoice/:id', component: InvoiceForm},
    {
      path: '/invoice/:id/edit', component: InvoiceForm, meta: {mode: 'edit'}},
  ]
})
