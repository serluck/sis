import axios from 'axios'
let mixins = {
  methods: {
    fetchData(data) {
      var vm = this
      axios.get('http://localhost:8000/api/' + data +'/')
        .then(function(response) {
          vm[data] = response.data
          // vm.$set(vm.$data, 'invoices', response.data)
        })
        .catch(function(error) {
          console.log(error)
        })
    }
  }
}
export default mixins